package org.my.multi.module.two.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Hello from App two (2) on application start.
 */
@Component
public class StartupBeanTwo {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {
        logger.info("App two (2) reporting in");
    }
}